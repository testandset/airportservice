# AirportService REST API :airplane:
--------------------------

[TOC]

## Overview 
-----------
AirportService is a Java application that serves a REST API to search for airports by IATA code or name. Application reads a CSV with airport information at start-up, loads it in a database and then queries it.

Service is hosted as a docker image on Amazon EC2 cluster. See full [API SPEC](http://ec2-35-176-51-220.eu-west-2.compute.amazonaws.com:8001) or try API directly at:

```
http://ec2-35-176-51-220.eu-west-2.compute.amazonaws.com:8001/airports/iata/sxf  (search by IATA code)
http://ec2-35-176-51-220.eu-west-2.compute.amazonaws.com:8001/airports?name=tegel  (search by name)
```

If you are running it locally, test service by,

```
http://localhost:8081/airports/iata/LHR  (search by IATA code)
http://localhost:8081/airports?name=ber  (search by name)
```

## API Spec
--------------------

See full API specification [**here**](http://ec2-35-176-51-220.eu-west-2.compute.amazonaws.com).

| Resource | Description |
| :---- | :------- |
| /airports/iata/{iataCode} | Search airports by IATA Code |
| /airports?name={name} | Search airports by name, returns all airports with names containing parameter|


By default service runs on 8081 port but it can be configured. See [configuration parameters](#markdown-header-configuration).

## Deployment and excution
------------------------------

### Docker image
To get up and running quickly get Docker image and run it in your container

Pull image

        docker pull 168780356778.dkr.ecr.eu-west-2.amazonaws.com/getchzx/airportservice

Run image

        docker run -p 8001:8001 168780356778.dkr.ecr.eu-west-2.amazonaws.com/getchzx/airportservice

## Docker file ##
Use the Docker file from repo *\target\docker* folder and create your own image.

Create docker image run on target folder

        docker build -t airportservice .

Run image

        docker run -p 8001:8001 ariportservice


### JAR Executable
If you are interested in running the JAR directly grab it from target folder and execute. All dependencies are in the JAR archive. You would need Java installed on your machine.

```
java -jar target\airportService-0.1.0.jar
```

### Build from source

To build the application from source install,

* Java 1.7
* Maven

Once you have installed the dependencies, checkout the code and run 

```
mvn package
```

This would generate executable JAR file in `target` folder. To [run](#markdown-header-jar-executable) the service execute JAR file.

If you have Docker set-up and Docker deamon is running, to create a Docker image run (run previous step first)

```
mvn package docker:build
```


### Running Tests

To run all the tests use Maven *test* goal. Maven *install* goal builds and run all the tests

```
mvn install
```

## Configuration ##
--------------------
Application supports fine control of configuration. There are two options to override default configuration,


1. By creating a application.properties file and putting it in the same folder as the executable JAR

    eg. to override default 8001 port:

        server.port=8080

1. By passing them as options in execute command

    eg. to override default 8001 port:

        java -jar target\airportService-0.1.0.jar --server.port=8080


### Parameters
1. *airports.csv* (filepath) : By default there is a CSV file packaged with application but if you want to override it pass the path of your file.
1. *response.pretty_print* (true/false) : To enable or disable response json pretty print
1. Database configuration
    1. To override database endpoints. Must specify all four
        1. *spring.datasource.url* : database url 
        1. *spring.datasource.username*: database username
        1. *spring.datasource.password*: database password
        1. *spring.datasource.driverClassName*: database driver to load
    1. To create auto DDL of CSV and load it on runtime
        1. *spring.jpa.generate-ddl* (true/false): To auto generate DDL
        1. *spring.jpa.properties.hibernate.hbm2ddl-auto* (none/auto): To presist in-memory database and load it on server restart
1. *debug* : To start debug level logging. By default only ERROR  are logged.
1. *server.port* (port-number): Override server port.

## Production Monitering
----------------------------

Application provides additional features to help you monitor production. Auditing, health and metrics gathering can be automatically applied to your application. 

For security reason they are on a different port from the REST API. Port of monitoring end-points is 9001. If you are running as Docker image make sure to map 9001 port to a container port.

There are following additional end-points

 1. Application health (/health) : Shows application health information.

   Sample response

		{
		  "status" : "UP",
		  "diskSpace" : {
			"status" : "UP",
			"total" : 155382525952,
			"free" : 100458762240,
			"threshold" : 10485760
		  },
		  "db" : {
			"status" : "UP",
			"database" : "H2",
			"hello" : 1
		  }
		}


 2. Application metrics (/metrics): Shows ‘metrics’ information for the current application.

    Sample response

		{
		  "mem" : 894895,
		  "mem.free" : 444115,
		  "processors" : 4,
		  "instance.uptime" : 599239,
		  "uptime" : 610279,
		  "systemload.average" : -1.0,
		  "heap.committed" : 836096,
		  "heap.init" : 128157,
		  "heap.used" : 391980,
		  "heap" : 1824256,
		  "nonheap.committed" : 89984,
		  "nonheap.init" : 24000,
		  "nonheap.used" : 58799,
		  "nonheap" : 133120,
		  "threads.peak" : 35,
		  "threads.daemon" : 32,
		  "threads.totalStarted" : 42,
		  "threads" : 35,
		  "classes" : 9476,
		  "classes.loaded" : 9476,
		  "classes.unloaded" : 0,
		  "gc.ps_scavenge.count" : 13,
		  "gc.ps_scavenge.time" : 194,
		  "gc.ps_marksweep.count" : 1,
		  "gc.ps_marksweep.time" : 536,
		  "cache.airportsByIataCode.size" : 1,
		  "httpsessions.max" : -1,
		  "httpsessions.active" : 0,
		  "datasource.primary.active" : 0,
		  "datasource.primary.usage" : 0.0,
		  "counter.status.200.airports.iata.iataCode" : 1,
		  "counter.status.200.star-star.favicon.ico" : 2,
		  "counter.status.304.airports.iata.iataCode" : 1,
		  "gauge.response.airports.iata.iataCode" : 15.0,
		  "gauge.response.star-star.favicon.ico" : 8.0
		}


## Error codes
----------------

Following error codes are logged by application,

1. **AS1000** : Failed to load CSV.

    Reasons:

    * Cannot find CSV you provided.
    * CSV is corrupt and can't read.
    * CSV has a incorrect schema. For it to load a CSV successfully it must have following headers with column names. All values must be <=255 characters except keywords which can be upto 1000.
        * id (must be unique)
        * ident
        * type
        * name
        * latitude_deg
        * longitude_deg
        * elevation_ft
        * continent
        * iso_country
        * iso_region
        * municipality
        * scheduled_service
        * gps_code
        * iata_code
        * local_code
        * home_link
        * wikipedia_link
        * keywords

2. **AS10010**: No results in database.

    Reasons:

    * Cannot connect to database
    * CSV failed to load

## Architecture and Design 
--------------------------
AirportService is based on Microservice Architectural pattern with REST API. It's a Spring application with embedded Tomcat application server, embedded H2 database and built in EH cache.

![airportservice.png](https://bitbucket.org/repo/oLogG9B/images/1893812264-airportservice.png)

### Design decisions

#### API Design

1. API has only one logical resource, Aiports. It's appropriately named */airports*
1. As IATA names are unique they uniquely identify airports. For search by IATA code API opens sub-resource *iata* (/airports/iata/{iataCode}). Iata code then becomes a path parameter and every airport resouce can be uniquly hyperlinked. This could be used later on in future if API grows and needs to support Hateoas.
1. For search by name query paramters are more suitable as it represents a filter.
1. All endpoint inputs are validated and sanitized. Malformed or invalid requests return `400 BAD REQUEST` with a error message.
    * IATA endpoint restricts iataCode input to 3 letter word.
    * Search by name restricts name parameter to 3 to 40 letters. 
1. All sucessful GET responses return `200 OK` response (except requests with `If-None-Match`)
1. IATA endpoint returns `404 NOT FOUND` when no resource representation is found.
1. Search by name endpoint return empty list when no airports are found with containing name. As filters are conceptually different from GET on id, empty array is appropriate than 404.
1. Both API endpoints are GET so caching can be heavily utilized to reduce network bandwidth and server load.
1. To support browser and CDN caching the API has `Cache-Control` headers. Currently it's only set to expire after an hour but it could be increased considerabley as Airport resource representation rarely change at best. 
1. API supports `ETag` caching for conditional caching. Any request with `If-None-Match` with same ETag returns blank response with `304 NOT MODIFIED` response.
1. Though the assignment brief mentions response to be pretty printed it can be turned off by configuration parameters. Pretty print increases computational overload and increase in payload so its best off turned off in production.
1. All `500 INTERNAL SERVER ERROR` return error objects with message *service error* and hides any implementation detail leakage.

#### Application Design
1. Sample space for applcation is quite small as it only servers airports with IATA code. Being a unique 3 letter code that limits the total number of airports to 17,576 (26 * 26 * 26).
1. As the sample space is small we can use an embedded in-memory database to save the application footprint. Being in-memory it would be orders faster than a traditional database. Application uses production ready [E2](http://www.h2database.com/html/main.html) embedded database. Its small, open-source and has full support for SQL.
    * Having embedded database makes deploying very straightforward as there are no dependencies and could scale very well in a clustered set-up.
    * While the choice was in-memory db, application can be seamlessly integrated with any other traditional database like PostgreSQL or MySQL just by changing application configuration.
   
1. Again given the sample space application rebuilds in-meomory database every time it starts. Keeping CSV file as single source of truth it manages integrity of data. It also makes updating to newer CSV versions easy. However, application can be easily configured to persist data or to create auto DDLs and use it on subsquent restarts to rebuild if its preffered.
1. As name seraches are computational and comparatively slower, caching searches on server end offers immediate benefit. Application uses [Ehcache](http://www.ehcache.org/) to boost performance and simplify scalability. It supports upto a Terabyte of cache but our use case can work with much smaller.
1. Both search by IATA and search by name endpoints go via cache.
1. Server side caching eviction strategy is *Least Frequently Used* , this is better strategy in our use case as it will keep most popular searches in cache and serve more clients.


### Future enhancements
1. For this particular use-case we can even remove database entirely; we can keep all the data in memory and serve it directly.
    * For IATA endpoint we can have an `AVL Tree` or `Trie` sorted with IATA code as key. This would give us `O(logn)` order of growth given the max upper limit it translates to a constant. Which is pretty much what database gives but saves the time it takes to execute the query and network delay.
    * For name search endpoint we have a multithreaded executor to execute a pool of threads to search in airport by *Boyer–Moore* string search algorithm which powers Grep. It's natively available in Java, which helps. We can assign 1000 names to each thread which will be able to churn them pretty quickly. Boyer-Moore has `O(n+m)` (n: length(textToSearch), m:length(textToSearchIn)), even by taking 100 letter airport name on average, this gives us pretty close to contant order of growth for search.
1. Current server cold start is sub 15 secs (on my modest Corei5 8GB machine). Though its not too terrible it can be again be enhanced by doing a multithreaded load of CSV objects to database. A pool of threads managed by an Executor could load a large set of data fairly quickly.
1. If the sample grows considerabley a clustured document based database like Redis would suit better. Data can be distributed across clusters and would scale well.
1. In line with clustered database we could use clustered caching like memcached to scale caching.
1. Current version does not validate CSV schema. It is tolerent to the order of the columns but it does not validate if it exactly matches. It logs error code `AS1000`if it fails to load schema. Further validations can be build it for more robust CSV load.
1. Application also does not validate for unique IATA names. As there is no way it can know which is correct one it simply loads all entries but returns the first one it finds when a search by IATA is made.

### Automated Testing
----------------------
There is complete code coverage by unit tests and end-to-end integration tests.

1. End-to-end integration tests checks all end points and all scenarios. By default they run when you build the application but can be executed mannually by maven `integration-test` goal.
    * [IATA Code Endpoint tests](src/test/java/aero/paxlife/rs/AirportServiceFindByIataIntegrationTest.java)
    * [Find by name Code Endpoint tests](src/test/java/aero/paxlife/rs/AirportServiceFindByNameIntegrationTest.java)
1. Other unit tests for all classes are [here](src/test/java/aero/paxlife)



------------------
**Made with**

[![java.png](https://bitbucket.org/repo/oLogG9B/images/2347355513-java.png)](https://java.com)
[![spring.png](https://bitbucket.org/repo/oLogG9B/images/1724981571-spring.png)](https://spring.io/)
[![apache_tomcat.png](https://bitbucket.org/repo/oLogG9B/images/3966467291-apache_tomcat.png)](http://tomcat.apache.org/)
[![EhcacheTwitterIcon.png](https://bitbucket.org/repo/oLogG9B/images/2823375531-EhcacheTwitterIcon.png)](http://www.ehcache.org/)
[![h2_db_48_49.png](https://bitbucket.org/repo/oLogG9B/images/777933094-h2_db_48_49.png)](http://www.h2database.com)


[![octocat.png](https://bitbucket.org/repo/oLogG9B/images/2874900668-octocat.png)](https://git-scm.com/)
[![docker.png](https://bitbucket.org/repo/oLogG9B/images/292493364-docker.png)](https://www.docker.com/)
[![SSIS-Amazon-S3-Cloud-Task.png](https://bitbucket.org/repo/oLogG9B/images/237402500-SSIS-Amazon-S3-Cloud-Task.png)](http://aws.amazon.com)
[![Swagger.png](https://bitbucket.org/repo/oLogG9B/images/19826479-Swagger.png)](http://swagger.io/swagger-editor/)