package aero.paxlife;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * Main application class. This is where it begins. Bootstraps application with application context and initiates it.
 * 
 * @author Deepak
 *
 */
@SpringBootApplication
@ImportResource("applicationContext.xml")
public class AirportSearchService {

    public static void main(String[] args) {
        SpringApplication.run(AirportSearchService.class, args);
    }
}