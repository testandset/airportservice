package aero.paxlife.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Central class to read all application config.
 * 
 * @author Deepak
 *
 */
@Component
public class AirportServiceConfig {

	@Value("${response.pretty_print}")	
	boolean prettyPrint;
	
	@Value("${airports.csv}")
	String airportsCsvFile;
	
	public boolean isPrettyPrintEnabled(){
		return prettyPrint;
	}
	
	public String getAirportFileLocation(){
		return airportsCsvFile;
	}	
}
