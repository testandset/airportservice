package aero.paxlife.config;

import java.util.Map;

import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;

/**
 * Custom error object representation. Hides exception parameters to prevent implementation detail leakage.
 * 
 * @author Deepak
 *
 */
@Component
public class ServiceErrorAttributes extends DefaultErrorAttributes {
 
	/**
	 * Remove Exception property from error object.
	 */
    @Override
    public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {
        Map<String, Object> errorAttributes = super.getErrorAttributes(requestAttributes, includeStackTrace);
        errorAttributes.remove("exception");
        return errorAttributes;
    }
}