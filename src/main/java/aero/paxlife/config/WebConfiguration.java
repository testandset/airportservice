package aero.paxlife.config;

import javax.servlet.Filter;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.ShallowEtagHeaderFilter;

/**
 * Configuration for application. Enables caching by adding Etag caching.
 * @author Deepak
 *
 */
@Configuration
@EnableCaching
public class WebConfiguration {
	
	/**
	 * Add ETag header by shallow checking.
	 * @return
	 */
    @Bean
    public Filter shallowEtagHeaderFilter() {
        return new ShallowEtagHeaderFilter();
    }
}