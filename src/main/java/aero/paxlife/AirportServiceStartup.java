package aero.paxlife;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import aero.paxlife.coreservice.AirportDBInitializer;

/**
 * Startup tasks for service. Loads CSV file into database
 * 
 * @author Deepak
 *
 */
@Component
public class AirportServiceStartup implements ApplicationListener<ApplicationReadyEvent> {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AirportDBInitializer dbInitializer;

	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event) {
		logger.info("Initializing database...");
		
		dbInitializer.initializeDb();
		
		logger.info("Database initialized");
	}

}
