package aero.paxlife.rs;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import aero.paxlife.config.AirportServiceConfig;
import aero.paxlife.coreservice.AirportService;
import aero.paxlife.coreservice.InputValidator;
import aero.paxlife.coreservice.domain.Airport;
import aero.paxlife.coreservice.domain.AirportMixin;

/**
 * Main resource class for airports resource. 
 * Currently supports end-points 
 * 	GET /airports/iata 
 *  Get /airports?name=
 * 
 * Add all future HTTP verbs here for /airports
 * 
 * @author Deepak
 */
@RestController
@RequestMapping("/airports")
public class AirportsResource {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AirportServiceConfig config;

	@Resource(name = "airportService")
	private AirportService service;

	@Resource(name = "validator")
	private InputValidator validator;

	/**
	 * Cache control header to cache for an hour.
	 */
	private CacheControl cacheControl = CacheControl.maxAge(1, TimeUnit.HOURS);

	/**
	 * End-point for GET /airport/iata/{iataCode} Returns single airport found
	 * with matching IATA code. For no results returns 404.
	 * 
	 * @param iataCode
	 *            String
	 * @return Airport
	 */
	@RequestMapping(value = "/iata/{iataCode}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Airport> getAirportByIATACode(@PathVariable String iataCode) {

		logger.debug("/GET /iata/" + iataCode);

		validator.validateIata(iataCode);

		return ResponseEntity
				.ok() // status
				.cacheControl(cacheControl) // cache header
				.body(service.getAirportByIataCode(iataCode)); // found airport
	}

	/**
	 * End-point for GET /airport?name={name} Returns list of airports with name
	 * containing the query parameter. When no airports are found with matching
	 * query returns an empty list.
	 * 
	 * @param name
	 *            String
	 * @return list List<Airprort>
	 */
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Airport>> getAirportsByName(@RequestParam("name") String name) {

		logger.debug("/GET ?name=" + name);

		validator.validateName(name);

		return ResponseEntity
				.ok() // status
				.cacheControl(cacheControl)  //caching header
				.body(service.getAirportByName(name)); // found airports
	}

	/**
	 * Custom builder for Airport object to only include properties specified in
	 * specification. It also enables response pretty printing.
	 * 
	 * @return
	 */
	@Bean
	public Jackson2ObjectMapperBuilder jacksonBuilder() {
		Jackson2ObjectMapperBuilder jasonMapper = new Jackson2ObjectMapperBuilder();

		// AirportMixin specifies which properties to pick
		jasonMapper.indentOutput(config.isPrettyPrintEnabled()).mixIn(Airport.class, AirportMixin.class);
		return jasonMapper;
	}

}
