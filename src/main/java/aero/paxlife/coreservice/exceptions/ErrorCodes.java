package aero.paxlife.coreservice.exceptions;

/**
 * Application error codes.
 * 
 * @author Deepak
 */
public enum ErrorCodes {
	
	FAILED_TO_LOAD_CSV("AS1000"),
	NO_RESULTS_IN_DB("AS10010");
	
	private final String errorCode;
	
	private final String errorDescription;
	
	ErrorCodes(String errorCode){
		this.errorCode = errorCode;
		this.errorDescription = name();
	}
	
	public String getErrorCode(){
		return errorCode;
	}
	
	public String getErrorDescription(){
		return errorDescription;
	}
	
	public String toString(){
		return errorCode + ":" + errorDescription;
	}
}
