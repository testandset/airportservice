package aero.paxlife.coreservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception for no airport found. Set to return 404 HTTP status
 * 
 * @author Deepak
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NoAirportFoundExeption extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public NoAirportFoundExeption(String search) {
		super("No airports found for " + search);
	}
}
