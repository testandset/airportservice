package aero.paxlife.coreservice.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception for all business errors. Sets 500 HTTP status.
 * 
 * @author Deepak
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class BusinessException extends RuntimeException {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final long serialVersionUID = 1L;
	
	private ErrorCodes errorCode;
	
	private String errorMessage;

	public BusinessException(ErrorCodes errorCode, String errorMessage) {
		super("Service unavailable");
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		
		logger.error(errorCode.toString());
		logger.error(errorMessage);
	}

	public ErrorCodes getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCodes errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}	
}
