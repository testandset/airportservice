package aero.paxlife.coreservice.db;

import java.util.List;

import aero.paxlife.coreservice.domain.Airport;

/**
 * Interface for AirportStore
 * 
 * @author Deepak
 */
public interface AirportStore {

	/**
	 * Add airport to store
	 * @param airport
	 */
	public void add(Airport airport);
	
	/**
	 * Find airport by IATA code
	 * @param iataCode
	 * @return
	 */
	public Airport findAirportByIataCode(String iataCode);
	
	/**
	 * Find airport by name
	 * @param name
	 * @return
	 */
	public List<Airport> findAirportByName(String name);
	
	/**
	 * Check if store is initialised successfully.
	 */
	public void checkIfInitializedSuccessfully();
}
