package aero.paxlife.coreservice.db;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import aero.paxlife.coreservice.domain.Airport;
import aero.paxlife.coreservice.exceptions.BusinessException;
import aero.paxlife.coreservice.exceptions.ErrorCodes;
import aero.paxlife.coreservice.exceptions.NoAirportFoundExeption;

/**
 * Concrete implementation for Airport store as a Data acess object.
 * 
 * @author Deepak
 */
@Repository
public class AirportDao implements AirportStore {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final String QUERY_FIND_BY_IATA_CODE = "SELECT * FROM airports WHERE UPPER(iata_code) = UPPER(?)";
	private final String QUERY_FIND_BY_NAME = "SELECT * FROM airports WHERE UPPER(name) like UPPER(concat('%', ?, '%'))";
	private final String QUERY_TEST_DB = "SELECT * FROM airports where rownum = 1";
	
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Add airport to table.
	 */
	public void add(Airport airport) {
		entityManager.merge(airport);
	}

	/**
	 * Find airport by IATA code.
	 * 
	 * All calls go via cache, if they are in cache its returned from there else it goes
	 * to database. All cache misses are then updated back in cache
	 */
	@Cacheable(value="airportsByIataCode", key="#iataCode")
	public Airport findAirportByIataCode(String iataCode) {
		
		logger.debug("findAirportByIataCode: " + iataCode);
		
		Query query = entityManager.createNativeQuery(QUERY_FIND_BY_IATA_CODE, Airport.class);
		query.setParameter(1, iataCode);

		List<Airport> airports = (List<Airport>) query.getResultList();

		if (airports.size() == 0) {
			throw new NoAirportFoundExeption(iataCode);
		}

		// As we don't validate for IATA uniqueness, just return the first airport found with IATA code
		return airports.get(0);
	}

	/**
	 * Find airport by name
	 * 
	 * All calls go via cache, if they are in cache its returned from there else it goes
	 * to database. All cache misses are then updated back in cache
	 */
	@Cacheable(value="airportsByName", key="#name")
	public List<Airport> findAirportByName(String name) {
		
		logger.debug("findAirportByName: " + name);
		
		Query query = entityManager.createNativeQuery(QUERY_FIND_BY_NAME, Airport.class);
		query.setParameter(1, name);

		List<Airport> airports = (List<Airport>) query.getResultList();

		return airports;
	}

	/**
	 * Checks if there are entries in table. Throws BusinessException if not.
	 */
	public void checkIfInitializedSuccessfully() {
		Query query = entityManager.createNativeQuery(QUERY_TEST_DB, Airport.class);

		try {
			query.getSingleResult();
		} catch (Exception e) {
			throw new BusinessException(ErrorCodes.NO_RESULTS_IN_DB, "Check if CSV schema is correct");
		}
	}
}
