package aero.paxlife.coreservice;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import aero.paxlife.coreservice.exceptions.BadRequestException;

/**
 * Input validator. Checks inputs for validity.
 * 
 * @author Deepak
 *
 */
@Component
public class InputValidator {
	
	/**
	 * Checks if IATA code
	 * 	- is only letters
	 *  - is only 3 characters long
	 *
	 * @param iataCode
	 */
	public void validateIata(String iataCode){
		if(!StringUtils.isAlpha(iataCode) || iataCode.trim().length() != 3){
			throw new BadRequestException("Iata code should only be 3 letter word [a-zA-Z]{3}. Ex. LHR");
		}
	}
	
	/**
	 * Validates name
	 * 		- Tests if its all letters
	 * 		- Tests length is > 3 and < 50
	 * 
	 * @param name
	 */
	public void validateName(String name){
		
		if(!StringUtils.isAlpha(name) || StringUtils.isBlank(name) || name.trim().length() > 50 || name.trim().length() < 3){
			throw new BadRequestException("Name paramter should only be non empty word [a-zA-Z]{3,50}");
		}
		
	}

}
