package aero.paxlife.coreservice.loader;

import java.util.List;

/**
 * Interface for file loader.
 * @author Deepak
 */
public interface FileLoader {

	/**
	 * Load objects from specified file
	 * @param type Object class to read
	 * @param fileName file name
	 * @return objects
	 */
	public <T> List<T> loadObjectList(Class<T> type, String fileName);
	
}
