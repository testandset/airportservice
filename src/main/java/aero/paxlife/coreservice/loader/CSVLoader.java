package aero.paxlife.coreservice.loader;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import aero.paxlife.coreservice.exceptions.BusinessException;
import aero.paxlife.coreservice.exceptions.ErrorCodes;

/**
 * CSV file loader. Reads a CSV files and type of objects to read it from.
 * 
 * @author Deepak
 */
@Component
public class CSVLoader implements FileLoader {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * Implementation of abstract method to read CSV files
	 */
	public <T> List<T> loadObjectList(Class<T> type, String fileName) {
		
		logger.debug("Reading file :" + fileName);
		
		List<T> list;
	    try {
	        list = readCSV(type, fileName);
	    } catch (Exception e) {
	    	throw new BusinessException(ErrorCodes.FAILED_TO_LOAD_CSV, e.getMessage());
	    }
	    
	    if(null == list || list.isEmpty()){
	    	throw new BusinessException(ErrorCodes.NO_RESULTS_IN_DB, "No valid entries in DB");
	    }
	    
	    return list;
	}
	
	/**
	 * Read objects from file.
	 * 
	 * @param type
	 * @param fileName
	 * @return
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	protected <T> List<T> readCSV(Class<T> type, String fileName) throws JsonProcessingException, IOException {
		CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
		CsvMapper mapper = new CsvMapper();
		ResourceLoader resourceLoader = new FileSystemResourceLoader();
		Resource resource = resourceLoader.getResource(fileName);
		InputStream file = resource.getInputStream();
		MappingIterator<T> readValues = mapper.readerFor(type).with(bootstrapSchema).readValues(file);

		return readValues.readAll();
	}
}
