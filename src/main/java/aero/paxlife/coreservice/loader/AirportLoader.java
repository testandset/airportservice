package aero.paxlife.coreservice.loader;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import aero.paxlife.config.AirportServiceConfig;
import aero.paxlife.coreservice.domain.Airport;

/**
 * Loads list of airports from configured fileLoader.
 * 
 * @author Deepak
 */
@Component
public class AirportLoader {
	
	@Autowired
	private AirportServiceConfig config;
	
	@Resource(name = "fileLoader")
	private FileLoader loader;
	
	/**
	 * Load airports from specified file in config
	 * @return list of airports
	 */
	public Collection<Airport> getAirports(){
		return loader.loadObjectList(Airport.class, config.getAirportFileLocation());
	}
}
