package aero.paxlife.coreservice;

import java.util.List;

import aero.paxlife.coreservice.domain.Airport;

/**
 * Interface for apirport service
 * 
 * @author Deepak
 */
public interface AirportService {

	public Airport getAirportByIataCode(String iataCode);
	
	public List<Airport> getAirportByName(String name);
}
