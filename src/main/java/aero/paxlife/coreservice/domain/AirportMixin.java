package aero.paxlife.coreservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Custom representation for airpor resource. 
 * 	- Sets property order
 * 	- Hides the ones that are not needed by API spec
 *  - Changes attribute names as per spec
 * 
 * @author Deepak
 */
@JsonPropertyOrder({ "name", "iata", "icao", "city", "country", "latitude", "longitue" })
@JsonIgnoreProperties({ "id", "type", "elevation_ft", "continent", "iso_region", "scheduled_service", "gps_code",
		"local_code", "home_link", "wikipedia_link", "keywords" })
public interface AirportMixin {

	@JsonProperty("name")
	abstract String getName();

	@JsonProperty("iata")
	abstract String getIata_code();

	@JsonProperty("icao")
	abstract String getIdent();

	@JsonProperty("city")
	abstract String getMunicipality();

	@JsonProperty("country")
	abstract String getIso_country();

	@JsonProperty("latitude")
	abstract String getLatitude_deg();

	@JsonProperty("longitude")
	abstract String getLongitude_deg();
}
