package aero.paxlife.coreservice;

import java.util.Collection;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aero.paxlife.coreservice.db.AirportStore;
import aero.paxlife.coreservice.domain.Airport;
import aero.paxlife.coreservice.loader.AirportLoader;

/**
 * DB initializer. Reads configured CSV database and loads all objects to database.
 * 
 * @author Deepak
 */
@Service
@Transactional
public class AirportDBInitializer {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	AirportLoader loader;

	@Resource(name="airportStore")
	private AirportStore airportStore;

	/**
	 * Load all airports from configured file
	 */
	public void initializeDb() {
		Collection<Airport> airports = loader.getAirports();

		for (Airport airport : airports) {
			try {
				if(!StringUtils.isBlank(airport.getIata_code())){
					logger.debug("Added: " + airport.toString());
					airportStore.add(airport);
				}
					
			} catch (Exception e) {
				//Ignore updating this airport
				logger.error("Failed to load " + airport.toString());
			}
		}
		
		// finally check if it all worked.
		airportStore.checkIfInitializedSuccessfully();
	}
	
	

}
