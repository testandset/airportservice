package aero.paxlife.coreservice;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import aero.paxlife.coreservice.db.AirportStore;
import aero.paxlife.coreservice.domain.Airport;

/**
 * Concrete implementation for AirportService via querying database.
 * 
 * @author Deepak
 */
@Component
public class AirportServiceInMemoryDBImpl implements AirportService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Resource(name="airportStore")
	private AirportStore airportStore;
	
	/**
	 * Find by iata code
	 */
	@Override
	public Airport getAirportByIataCode(String iataCode) {
		
		logger.debug("getAirportByIataCode:" + iataCode);
		
		return airportStore.findAirportByIataCode(iataCode);
	}

	/**
	 * Find by name
	 */
	@Override
	public List<Airport> getAirportByName(String name) {
		
		logger.debug("getAirportByName:" + name);
		
		return airportStore.findAirportByName(name);
	}

}
