package aero.paxlife.coreservice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import aero.paxlife.coreservice.exceptions.BadRequestException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(InputValidator.class)
public class InputValidatorTest {
	
	@Autowired
	InputValidator inputValidator;

	@Test
	public void testValidIata(){
		inputValidator.validateIata("ABC");
	}

	@Test(expected=BadRequestException.class)
	public void testIataCodeisAlpha() {
		inputValidator.validateIata("234");
	}
	
	@Test(expected=BadRequestException.class)
	public void testIataCodeLengthViolation_lessThan() {
		inputValidator.validateIata("AA");
	}
	
	@Test(expected=BadRequestException.class)
	public void testIataCodeLengthViolation_greaterThan() {
		inputValidator.validateIata("AAaaA");
	}
	
	@Test
	public void testValidName(){
		inputValidator.validateIata("ABC");
	}
	
	@Test(expected=BadRequestException.class)
	public void testNameIsAlpha() {
		inputValidator.validateName("456");
	}
	
	@Test(expected=BadRequestException.class)
	public void testNameLengthViolation_lessThan() {
		inputValidator.validateName("AA");
	}
	
	@Test(expected=BadRequestException.class)
	public void testNameLengthViolation_greaterThan() {
		inputValidator.validateName("AaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaA");
	}
}
