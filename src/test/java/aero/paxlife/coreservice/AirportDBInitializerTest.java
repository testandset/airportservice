package aero.paxlife.coreservice;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import aero.paxlife.coreservice.db.AirportStore;
import aero.paxlife.coreservice.domain.Airport;
import aero.paxlife.coreservice.loader.AirportLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration({AirportDBInitializer.class})
@ContextConfiguration("/applicationContext.xml")
@TestPropertySource("classpath:test.properties")
public class AirportDBInitializerTest {

	@InjectMocks
	AirportDBInitializer airportDbInitializer;

	@Mock
	AirportLoader loader;	

	@Mock
	AirportStore airportStore;
	
	@Before
    public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}	

	/**
	 * Test database is loaded
	 */
	@Test
	public void testDbIsLoader() {
		Airport airport = new Airport();
		airport.setIata_code("LHR");
		
		List<Airport> airports = new ArrayList<Airport>();
		airports.add(airport);
		
		when(loader.getAirports()).thenReturn(airports);
		
		airportDbInitializer.initializeDb();
		
		verify(airportStore, times(1)).add(airport);		
	}

}
