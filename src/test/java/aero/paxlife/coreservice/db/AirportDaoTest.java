package aero.paxlife.coreservice.db;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import aero.paxlife.coreservice.domain.Airport;
import aero.paxlife.coreservice.exceptions.BusinessException;
import aero.paxlife.coreservice.exceptions.NoAirportFoundExeption;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration({AirportDao.class})
@ContextConfiguration("/applicationContext.xml")
@TestPropertySource("classpath:test.properties")
public class AirportDaoTest {

	@Autowired
	AirportStore airportStore;

    @Test
    @Transactional
    @Rollback(true)
    public void testAddDepartment()
    {
    	Airport airport = new Airport();
    	airport.setId("12345");
    	airport.setIata_code("XXX");

    	airportStore.add(airport);
    	
    	Assert.isTrue(airport.getIata_code().equals(airportStore.findAirportByIataCode("XXX").getIata_code()));
    }
    
    @Test
    @Transactional
    @Rollback(true)
    public void testfindAirportByIataCode()
    {
    	Airport airport = getAirport();
    	airportStore.add(airport);
    	Assert.isTrue(airportStore.findAirportByIataCode(airport.getIata_code())
    			.getIata_code().equals(airport.getIata_code()));
    }
    
    @Test(expected=NoAirportFoundExeption.class)
    @Transactional
    public void testfindAirportByIataCodeForNoResults()
    {
    	String iataCode = "LHR";
    	Assert.isTrue(airportStore.findAirportByIataCode(iataCode).getIata_code().equals(iataCode));
    }
    
    @Test
    @Transactional
    @Rollback(true)
    public void testfindAirportByIataCodeForCaseInsensitive()
    {
    	Airport airport = getAirport();
    	airportStore.add(airport);
    	Assert.isTrue(airportStore.findAirportByIataCode(airport.getIata_code().toLowerCase())
    			.getIata_code().equalsIgnoreCase(airport.getIata_code()));
    }

    @Test
    @Transactional
    public void testfindAirportByName()
    {
    	Airport airport = getAirport();
    	airportStore.add(airport);
    	Assert.isTrue(airportStore.findAirportByName(airport.getName().substring(1, 5)).get(0)
    			.getName().equals(airport.getName()));
    }

    @Test
    @Transactional
    public void testfindAirportByNameForCaseInsensitive()
    {
    	Airport airport = getAirport();
    	airportStore.add(airport);
    	Assert.isTrue(airportStore.findAirportByName(airport.getName().substring(1, 5).toLowerCase()).get(0)
    			.getName().equals(airport.getName()));
    }

    @Test
    @Transactional
    public void testfindAirportByNameForNoResults()
    {
    	String name = "Ber";
    	Assert.isTrue(airportStore.findAirportByName(name).isEmpty());
    }
    
    @Test(expected=BusinessException.class)
    @Transactional
    public void testcheckIfInitializedSuccessfully()
    {
    	airportStore.checkIfInitializedSuccessfully();
    }

    Airport getAirport(){
    	Airport airport = new Airport();
    	airport.setId("1234");
    	airport.setIata_code("LHR");
    	airport.setName("London Heathrow Airport");
    	return airport;
    }
}
