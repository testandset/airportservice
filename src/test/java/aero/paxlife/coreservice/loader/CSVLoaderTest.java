package aero.paxlife.coreservice.loader;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import aero.paxlife.coreservice.domain.Airport;
import aero.paxlife.coreservice.exceptions.BusinessException;
import aero.paxlife.coreservice.exceptions.ErrorCodes;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(CSVLoader.class)
public class CSVLoaderTest {

	@Autowired
	CSVLoader csvLoader;

	@Test
	public void loadsCSVSuccessfully() {
		List<Airport> airports = csvLoader.loadObjectList(Airport.class, "classpath:test-airports.csv");
		
		Assert.notEmpty(airports);
		Assert.isTrue(airports.size() == 5);		
	}
	
	@Test
	public void throwsBusinessExceptionWhenFileNotFound(){
		ErrorCodes errorCode = null;
		
		try{
			csvLoader.loadObjectList(Airport.class, "classpath:fileNotFound.csv");			
		} catch(BusinessException be){
			errorCode = be.getErrorCode();
		}
		
		Assert.isTrue(ErrorCodes.FAILED_TO_LOAD_CSV.equals(errorCode));
	}
	
	@Test
	public void throwsBusinessExceptionWhenFileEmpty(){
		ErrorCodes errorCode = null;
		
		csvLoader = new CSVLoader(){
			protected <T> List<T> readCSV(Class<T> type, String fileName){
				return new ArrayList<T>();
			}
		};
		
		try{
			csvLoader.loadObjectList(Airport.class, "classpath:file.csv");			
		} catch(BusinessException be){
			errorCode = be.getErrorCode();
		}
		
		Assert.isTrue(ErrorCodes.NO_RESULTS_IN_DB.equals(errorCode));
	}


}
