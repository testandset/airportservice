package aero.paxlife.rs;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.jayway.restassured.RestAssured;

import aero.paxlife.AirportSearchService;

/**
 * Integration test for iata endpoint.
 * @author Deepak
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(AirportSearchService.class)
@WebAppConfiguration
@IntegrationTest("spring.config.location=classpath:/test.properties")
public class AirportServiceFindByIataIntegrationTest {

	private static final String AIRPORT_BY_IATA_RESOURCE = "/airports/iata/";

	@Value("${local.server.port}")
	private int serverPort;

	@Before
	public void setUp() {
		RestAssured.port = serverPort;
	}

	@Test
	public void found() {		
		get(AIRPORT_BY_IATA_RESOURCE + "LHR")
		.then()
			.assertThat().statusCode(200)
			.assertThat().body("iata", equalTo("LHR"))
			.assertThat().body("name", equalTo("London Heathrow Airport"))
			.assertThat().body("icao", equalTo("EGLL"))
			.assertThat().body("city", equalTo("London"))
			.assertThat().body("country", equalTo("GB"))
			.assertThat().body("latitude", equalTo("51.4706"))
			.assertThat().body("longitude", equalTo("-0.461941"));			
	}
	
	@Test
	public void caseInsensitive() {		
		get(AIRPORT_BY_IATA_RESOURCE + "lhr")
		.then()
			.assertThat().statusCode(200)
			.assertThat().body("iata", equalTo("LHR"))
			.assertThat().body("name", equalTo("London Heathrow Airport"))
			.assertThat().body("icao", equalTo("EGLL"))
			.assertThat().body("city", equalTo("London"))
			.assertThat().body("country", equalTo("GB"))
			.assertThat().body("latitude", equalTo("51.4706"))
			.assertThat().body("longitude", equalTo("-0.461941"));			
	}

	
	@Test
	public void NotFound() {		
		get(AIRPORT_BY_IATA_RESOURCE + "NCL")
		.then()
			.assertThat().statusCode(404)
			.assertThat().body("message", not(isEmptyOrNullString()));
	}
	
	@Test
	public void invalidRequestInputGreaterThanLength3() {		
		get(AIRPORT_BY_IATA_RESOURCE + "ABCD")
		.then()
			.assertThat().statusCode(400)
			.assertThat().body("error", equalToIgnoringCase("Bad Request"))
			.assertThat().body("message", not(isEmptyOrNullString()));		
	}

	
	@Test
	public void hasETagHeader(){
		get(AIRPORT_BY_IATA_RESOURCE + "BER")
		.then()
			.assertThat().statusCode(200)
			.assertThat().header("ETag", not(isEmptyOrNullString()));
	}
	
	@Test
	public void hasCacheControlHeader(){
		get(AIRPORT_BY_IATA_RESOURCE + "BER")
		.then()
			.assertThat().statusCode(200)
			.assertThat().header("Cache-Control", containsString("max-age"));		
	}
	
	@Test
	public void returns304ForIfNoneMatchEtagRequest(){		
		String etag = get(AIRPORT_BY_IATA_RESOURCE + "BER").header("Etag");
		
		given().header("If-None-Match", etag)
			.get(AIRPORT_BY_IATA_RESOURCE + "BER")
			.then()
			.assertThat().statusCode(304);
	}
}